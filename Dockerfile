FROM rubdos/maven-javacard

RUN apt update; echo 0
RUN apt install -y pcscd \
    pcsc-tools \
    libpcsclite-dev

CMD service pcscd start && bash